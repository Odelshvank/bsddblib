import os
import marshal
import warnings

from hashlib import sha256
from codecs import unicode_escape_encode
from codecs import unicode_escape_decode

from ..lib import dumps_composite_key, loads_composite_key
from ..btree import inrange, fetchprefix
from .. import *


try:
    unicode
except NameError:
    unicode = str

literal_types = frozenset([
    bool,
    int,
    float,
    str,
    bytes,
    unicode,
    type(None)
])


def function_digest(f):
    code = f.__code__
    return sha256(marshal.dumps([
        code.co_argcount,
        code.co_nlocals,
        code.co_stacksize,
        code.co_flags,
        code.co_code,
        #code.co_consts,
        [x for x in code.co_consts if type(x) in literal_types],
        code.co_names,
        code.co_name,
        code.co_lnotab,
        code.co_freevars,
        code.co_cellvars
    ])).digest()


class IndiceRecreateWarning(RuntimeWarning):
    pass


class BTreeStorage(object):
    from marshal import dumps as _dumps
    from marshal import loads as _loads

    secondary_detect = True
    
    def __init__(self, dbenv, flags=0, dumps=None, loads=None):
        self._env = dbenv
        
        self._db = DB(dbenv, flags)
        self._db.set_flags(DB_RECNUM)
        
        self._dbs = DB(dbenv, flags)
        self._dbs.set_flags(DB_RECNUM)

        self._dbcfg = DB(dbenv, flags)
        
        if dumps: self._dumps = dumps
        if loads: self._loads = dumps

        encapsulate = getattr(self, 'encapsulate', None)
        decapsulate = getattr(self, 'decapsulate', None)
        
        if encapsulate:
            loads = self._loads
            self._loads = lambda x: encapsulate(loads(x))

        if decapsulate:
            dumps = self._dumps
            self._dumps = lambda x: decapsulate(dumps(x))
        
    def open(self, dbdir=None, dbfile=None, flags=DB_CREATE, mode=0, txn=None):
        '''
        '''
        if dbdir:
            path_db = os.path.join(dbdir, 'primary.db')
            path_dbs = os.path.join(dbdir, 'secondary.db')
            path_cfg = os.path.join(dbdir, 'cfg.db')
        elif dbfile:
            path_db = dbfile
            path_dbs = dbfile+'.secondary'
            path_cfg = dbfile+'.cfg'
        else:
            path_db = None
            path_dbs = None
            path_cfg = None
            
        self._db.open(path_db, DB_BTREE, flags, mode, txn)
        self._dbs.open(path_dbs, DB_BTREE, flags, mode, txn)
        self._dbcfg.open(path_cfg, DB_BTREE, flags, mode, txn)
        
        if self.secondary_detect and not flags & DB_RDONLY:
            old_digest = self._dbcfg.get(b'secondary_digest')
            new_digest = function_digest(self.secondary_keys)
            
            if old_digest and old_digest != new_digest:
                msg = \
                '%s.secondary_keys was changed. ' \
                'Secondary indice will be recreated. ' \
                'All cursors of this database must be closed.' % self.__class__.__name__
                warnings.warn(msg, IndiceRecreateWarning)
                self._dbs.truncate(txn)
                
            self._dbcfg.put(b'secondary_digest', new_digest, txn=txn)

        # Secondary keys extractor
        def callback(rawkey, rawdata):
            key = unicode_escape_decode(rawkey)[0]
            data = self._loads(rawdata)
            
            keys = []
            
            for k in self.secondary_keys(key, data):
                keys.append(dumps_composite_key(k))

            return keys

        self._db.associate(self._dbs, callback, DB_CREATE, txn)
        self.sync()

    def secondary_keys(self, key, data):
        '''
        '''
        return []
    
    def put(self, key, data, txn=None, flags=0):
        '''
        '''
        key = unicode_escape_encode(key)[0]
        self._db.put(key, self._dumps(data), txn=txn, flags=flags)
    
    def get(self, key, txn=None, flags=0):
        '''
        '''
        if isinstance(key, unicode):
            key = unicode_escape_encode(key)[0]
            value = self._db.get(key, txn=txn, flags=flags)
        else:
            key = dumps_composite_key(key)
            value = self._dbs.get(key, txn=txn, flags=flags)

        if value:
            return self._loads(value)

    def exists(self, key, txn=None, flags=0):
        '''
        '''
        if isinstance(key, unicode):
            key = unicode_escape_encode(key)[0]
            return self._db.exists(key, txn=txn, flags=flags)
        else:
            key = dumps_composite_key(key)
            return self._dbs.exists(key, txn=txn, flags=flags)

    def discard(self, key, txn=None):
        '''
        '''
        try:
            if isinstance(key, unicode):
                key = unicode_escape_encode(key)[0]
                self._db.delete(key, txn=txn)
            else:
                key = dumps_composite_key(key)
                self._dbs.delete(key, txn=txn)
        except KeyError:
            return False
        else:
            return True
    
    def fetch(self, key, count, offset=0, partial=False, txn=None, flags=0):
        '''
        '''
        is_primary = isinstance(key, unicode)
        
        if is_primary:
            start = unicode_escape_encode(key)[0]
            cursor = self._db.cursor(txn=txn, flags=flags | DB_CURSOR_BULK)
        elif partial:
            start = dumps_composite_key(key)[:-1]
            cursor = self._dbs.cursor(txn=txn, flags=flags | DB_CURSOR_BULK)
        else:
            start = dumps_composite_key(key)
            cursor = self._dbs.cursor(txn=txn, flags=flags | DB_CURSOR_BULK)

        try:
            if start:
                stop = bytearray(start)
                stop[-1] += 1
                stop = bytes(stop)
            else:
                stop = b''
            
            yield inrange(cursor, start, stop, with_upper=False)

            if is_primary:
                for key, data in fetchprefix(cursor, start, count, offset):
                    yield unicode_escape_decode(key)[0], self._loads(data)
            else:
                for key, data in fetchprefix(cursor, start, count, offset):
                    yield loads_composite_key(key), self._loads(data)
        finally:
            cursor.close()

    def clear(self, key, count, offset=0, partial=False, txn=None, flags=0):
        '''
        '''
        if isinstance(key, unicode):
            start = unicode_escape_encode(key)[0]
            cursor = self._db.cursor(txn=txn, flags=flags | DB_CURSOR_BULK)
        elif partial:
            start = dumps_composite_key(key)[:-1]
            cursor = self._dbs.cursor(txn=txn, flags=flags | DB_CURSOR_BULK)
        else:
            start = dumps_composite_key(key)
            cursor = self._dbs.cursor(txn=txn, flags=flags | DB_CURSOR_BULK)

        try:
            deleted = 0
            for key, data in fetchprefix(cursor, start, count, offset):
                cursor.delete()
                deleted += 1
            
            return deleted
        finally:
            cursor.close()

    def sync(self):
        '''
        '''
        self._db.sync()
        self._dbs.sync()
        self._dbcfg.sync()
        
    def truncate(self, txn=None):
        '''
        '''
        self._db.truncate(txn)
        
    def close(self):
        '''
        '''
        self._db.close()
        self._dbs.close()
        self._dbcfg.close()

