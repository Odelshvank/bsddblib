#include <Python.h>
#include <string.h>
#include <stdio.h>

/*
Static pool
*/
#define PY_OBJECTS_POOL_SIZE 2048
PyObject *PY_OBJECTS_POOL[PY_OBJECTS_POOL_SIZE];


/*
*/
void pool_decref(PyObject **pool, Py_ssize_t len){
	Py_ssize_t i = 0;
	
	for (i = 0; i < len; i++){
		Py_DECREF(pool[i]);
	}
}


/*
*/
PyObject *dumps_composite_key(PyObject *self, PyObject *list){
	PyObject **pool = 0;
	PyObject *key = 0;
	PyObject *item = 0;
	PyObject *encoded = 0;
	Py_ssize_t i = 0;
	Py_ssize_t list_size = 0;
	Py_ssize_t key_size = 0;
	
	if (!PyList_CheckExact(list)){
		PyErr_SetString(PyExc_TypeError, "value must be list");
		return 0;
	}
	
	list_size = PyList_Size(list);
	
	if (list_size > PY_OBJECTS_POOL_SIZE){
		PyErr_SetString(PyExc_ValueError, "list length must be < 2048");
		return 0;
	}
	
	pool = PY_OBJECTS_POOL;
	
	for (i = 0; i < list_size; i++){
		item = PyList_GET_ITEM(list, i);
		
		if (PyUnicode_CheckExact(item)){
			encoded = PyUnicode_AsUnicodeEscapeString(item);
			
			if (encoded){
				key_size += PyBytes_GET_SIZE(encoded) + 1; // + 1 for delimiter
				pool[i] = encoded;
			}
			else {
				pool_decref(pool, i);
				return 0;
			}
		}
		else {
			PyErr_SetString(PyExc_TypeError, "List items must be unicode strings");
			pool_decref(pool, i);
			return 0;
		}	
	}
	
	key = PyBytes_FromStringAndSize(0, key_size);
	
	if (key){
		char *key_buffer = PyBytes_AS_STRING(key);
		
		for (i = 0; i < list_size; i++){
			memcpy(key_buffer, PyBytes_AS_STRING(pool[i]), PyBytes_GET_SIZE(pool[i]));
			
			key_buffer += PyBytes_GET_SIZE(pool[i]);
			*key_buffer++ = '\1'; // delimiter
			
			Py_DECREF(pool[i]);
		}
		
		return key;
	}
	
	pool_decref(pool, list_size);
	return 0;
}


/*
*/
PyObject *loads_composite_key(PyObject *self, PyObject *key){
	char *begin = 0;
	char *final = 0;
	char *found = 0;
	char *key_buffer = 0;
	
	PyObject *list = 0;
	PyObject *decoded = 0;
	Py_ssize_t key_size = 0;
	
	
	if (!PyBytes_CheckExact(key)){
		PyErr_SetString(PyExc_TypeError, "value must be a byte string");
		return 0;
	}
	
	list = PyList_New(0);
	key_size = PyBytes_GET_SIZE(key);
	key_buffer = PyBytes_AS_STRING(key);
	
	begin = 0;
	final = key_buffer + key_size;
	found = key_buffer - 1;
	
	while (1) {
		begin = found + 1;
		found = memchr(begin, '\1', final - begin);
		
		if (found) {
			decoded = PyUnicode_DecodeUnicodeEscape(begin, found - begin, "strict");
		}
		else break;
		
		if (decoded) {
			PyList_Append(list, decoded);
			Py_DECREF(decoded);
		}
		else return 0;
		
	}
	
	return list;
}


static PyMethodDef clib_methods[] = {
    {"dumps_composite_key", (PyCFunction) dumps_composite_key, METH_O, "Convert list of key chunks to composite key."},
	{"loads_composite_key", (PyCFunction) loads_composite_key, METH_O, "Convert composite key into list of chunks."},
    {NULL, 0, 0, NULL}
};

#if PY_MAJOR_VERSION >= 3

static struct PyModuleDef moduledef = {
	PyModuleDef_HEAD_INIT,
	"clib",
	0,              /* m_doc */
	-1,             /* m_size */
	clib_methods,   /* m_methods */
	NULL,           /* m_reload */
	NULL,           /* m_traverse */
	NULL,           /* m_clear */
	NULL            /* m_free */
};

PyObject *PyInit_clib(void) {
	return PyModule_Create(&moduledef);
}

#else

PyMODINIT_FUNC initclib() {
    Py_InitModule("clib", clib_methods);

    if (PyErr_Occurred()) {
        PyErr_SetString(PyExc_ImportError, "clib module init failed");
		return;
    }
}

#endif

