# Copyright (c) 2017, Arthur Goncharuk
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
# OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import sys
import warnings

try:
    from bsddb3.db import *
except ImportError:
    warnings.warn('bsddb3 not found. Built-in module bsddb will be used.', ImportWarning)
    from bsddb.db import *


def counter_add(cursor, key, value, default=None, loads=int, dumps=bytes):
    '''
    Increases or decreases (depending on the value sign) the counter.
    If defaults is not None and key missing, default will be set.
    Returns True if the counter was putted, False otherwise.
    
    loads: callable(record) -> int
    dumps: callable(int) -> record
    '''
    record = cursor.set(key, flags=DB_RMW)
    
    if record:
        counter = loads(record[1])
        cursor.put(key, dumps(counter+value), flags=DB_CURRENT)
        return True
    elif default is not None:
        cursor.put(key, dumps(default), flags=DB_KEYFIRST)
        return True
    
    return False


class autocommit:
    '''
    '''
    class abort(Exception):
        pass
    
    def __init__(self, curs=[], txns=[]):
        self.curs, self.txns = curs, txns

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, tb):
        for cursor in self.curs:
            cursor.close()

        if exc_type is None:
            for txn in self.txns:
                txn.commit()
        
        elif exc_type is self.abort:
            for txn in self.txns:
                txn.abort()
            
            return True
    
        else:
            for txn in self.txns:
                txn.abort()


