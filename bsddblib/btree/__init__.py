try:
    xrange
except NameError:
    xrange = range


def inrange(cursor, start=b'', stop=b'', with_upper=True):
    '''
    '''
    if stop and start > stop:
        raise ValueError('stop must be >= start')
    
    if cursor.set_range(start, dlen=0, doff=0):
        lower = cursor.get_recno()

        if stop:
            record = cursor.set_range(stop, dlen=0, doff=0)
            
            if record:
                return cursor.get_recno() - lower + (with_upper and record[0].startswith(stop))
        
        cursor.last(dlen=0, doff=0)
        return cursor.get_recno() - lower + 1
    else:
        return 0


def fetchprefix(cursor, start, count, offset=0):
    '''
    '''
    record = cursor.set_range(start)

    if record:
        if offset:
            record = cursor.set_recno(cursor.get_recno() + offset)

        for i in xrange(count):
            if record and record[0].startswith(start):
                yield record
                record = cursor.next()
            else:
                return
