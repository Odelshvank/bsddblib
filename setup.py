from setuptools import setup, Extension, find_packages

sources = ['./bsddblib/clib.c']
clib = Extension(name='bsddblib.clib', sources=sources)

classifiers = [
    'Development Status :: 3 - Alpha',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: BSD License',
    'Programming Language :: C',
    'Programming Language :: Python :: 2.7'
]

setup(name='bsddblib',
      version='0.1.0',
      author='Arthur Goncharuk',
      license='BSD License',
      description = 'High-performance storage implementations based on Oracle Berkeley DB',
      packages=find_packages(),
      ext_modules=[clib],
      classifiers=classifiers
)




